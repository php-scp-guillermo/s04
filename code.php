<?php

//The keyword "public" below is an example of an access modifier
//properties and methods that are public can be directly accessed
//Keep in mind, when a property or method is accessed, not only can it be displayed, it can also be reassigned

//For security purposes, as a layer of abstraction, we can use the "private" keyword instead
//properties and methods that are private can NOT be directly accessed
//properties and methods that are private also cannot be inherited by child classes

//If you want a property or method to NOT be accessible but inheritable, "protected" can be used instead of private

class Building
{
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address)
    {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    //Encapsulation
    //Encapsulation allows private or protected properties/methods to be INDIRECTLY printed or changed with the use of getters and setters
    //Since getters and setters are encapsulated (within) the class itself, they can get around the restrictions of private/protected properties/methods

    //getters allow indirect access to print out values
    //getter names are user-defined (can be anything)
    public function getName()
    {
        return $this->name;
    }

    //setters allow indirect access to reassign values
    //setter names are user-defined (can be anything)
    public function setName($name)
    {
        return $this->name = $name;
    }

    //By convention, EVERY private/protected property should have a getter and a setter
    public function getFloors()
    {
        return $this->floors;
    }

    public function setFloors($floors)
    {
        $this->floors = $floors;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function printName()
    {
        return "The name of the building is $this->name";
    }
}

class Condominium extends Building
{
    public function printName()
    {
        return "The name of the condominium is $this->name";
    }
}

$building = new Building('Caswynn Building', 8, 'Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, "Makati City, Philippines");
