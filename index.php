<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S4: Access Modifiers and Encapsulation</title>

    <style>
        div {
            width: 600px;
            margin: 3rem auto;
            border: 5px solid black;
            padding: 1rem;
        }
    </style>
</head>

<body>
    <div>
        <h1>Building</h1>
        <p>The name of the building <?= $building->getName(); ?></p>
        <p>The <?= $building->getName() ?> has <?= $building->getFloors(); ?> floors.</p>
        <p>The <?= $building->getName() ?> is located at <?= $building->getAddress(); ?></p>
        <?php $building->setName('Caswynn Complex'); ?>
        <p>The name of the building hsa been changed to <?= $building->getName() ?></p>

        <h1>Condominium</h1>
        <p>The name of the building <?= $condominium->getName(); ?></p>
        <p>The <?= $condominium->getName() ?> has <?= $condominium->getFloors(); ?> floors.</p>
        <p>The <?= $condominium->getName() ?> is located at <?= $condominium->getAddress(); ?></p>
        <?php $condominium->setName('Enzo Tower'); ?>
        <p>The name of the building hsa been changed to <?= $condominium->getName() ?></p>


    </div>

</body>

</html>